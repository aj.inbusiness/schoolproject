from django.db import models
from django.contrib.auth.models import User

class ExtendUserModel(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    department = models.CharField('Department', max_length=12, null=True)

    def __str__(self):
        return self.user.username