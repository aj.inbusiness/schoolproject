from django import forms

class CustomizedUserForm(forms.Form):
    message = forms.CharField(
        label='Message',
        max_length=400,
        widget=forms.Textarea(
            attrs={'class': 'form-control message', 'id': 'message', 'placeholder': 'Write your mesage here...'}),
        help_text="Write your mesage here...",
    )

