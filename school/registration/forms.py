from django import forms
from django.contrib.auth.models import User
from django.contrib.auth.forms import UserCreationForm, UserChangeForm, AuthenticationForm, PasswordChangeForm, PasswordResetForm, SetPasswordForm
from .models import UserProfileModel

class RegistrationForm(UserCreationForm):

    first_name = forms.CharField(required=True, widget=forms.TextInput(attrs={'class': 'form-control', 'min_length': '3'}))
    last_name = forms.CharField(required=True, widget=forms.TextInput(attrs={'class': 'form-control'}))
    username = forms.CharField(widget=forms.TextInput(attrs={'class': 'form-control'}))
    email = forms.EmailField(widget=forms.EmailInput(attrs={'class': 'form-control'}))
    password1 = forms.CharField(label='Password', widget=forms.PasswordInput(attrs={'class': 'form-control'}))
    password2 = forms.CharField(label='Confirm Password', widget=forms.PasswordInput(attrs={'class': 'form-control'}))


    class Meta:
        model = User
        fields = ['first_name', 'last_name', 'email', 'username']
        #widgets = {'email': forms.EmailField(widget=forms.EmailField(attrs={'class': 'form-control'}))}
        User._meta.get_field('email')._unique = True

        def __init__(self, *args, **kwargs):
            super().__init__(*args, **kwargs)

class UserProfileEditForm(UserChangeForm):

    first_name = forms.CharField(required=True, widget=forms.TextInput(attrs={'class': 'form-control', 'min_length': '3'}))
    last_name = forms.CharField(required=True, widget=forms.TextInput(attrs={'class': 'form-control'}))
    username = forms.CharField(widget=forms.TextInput(attrs={'class': 'form-control'}))
    email = forms.EmailField(widget=forms.EmailInput(attrs={'class': 'form-control'}))

    def __init(self, *args, **kwargs):
        super(UserProfileEditForm, self).__init__(self, *args, **kwargs)
        del self.fields['password']

    class Meta:
        model = User
        #widgets = {'dob': forms.DateInput(format=('%m/%d/%Y'), attrs={'type': 'date'})}
        fields = ['first_name', 'last_name', 'email', 'username']
        #exclude = ['username']


class ChangePasswordForm(PasswordChangeForm):

    old_password = forms.CharField(label='Old Password', widget=forms.PasswordInput(attrs={'class': 'form-control'}))
    new_password1 = forms.CharField(label='New Password', widget=forms.PasswordInput(attrs={'class': 'form-control'}))
    new_password2 = forms.CharField(label='Confirm Password', widget=forms.PasswordInput(attrs={'class': 'form-control'}))

    def clean_old_password(self):
        try:
            return super(ChangePasswordForm, self).clean_old_password()
        except forms.ValidationError:
            raise forms.ValidationError("Your old password was entered incorrecltly.")
    
    def clean_new_password2(self):
        try:
            return super(ChangePasswordForm, self).clean_new_password2()
        except forms.ValidationError:
            raise forms.ValidationError("Your new password and confirm password doesn't match."); 

    class Meta:
        model = User
        fields = "__all__"

class CustomizeLogin(AuthenticationForm):
    pass

class CustomizeChangePassword(PasswordChangeForm):
    old_password = forms.CharField(label='Old Password')
    new_password1 = forms.CharField(label='New Password', widget=forms.PasswordInput(attrs={'class': 'form-control'}))
    new_password2 = forms.CharField(label='Confirm Password', widget=forms.PasswordInput(attrs={'class': 'form-control'}))


class CustomizeForgotPassword(PasswordResetForm):
    email = forms.EmailField(label = 'Email address', error_messages = {'invalid': 'Invalid Email address.'})

    def clean_email(self):
        email_id = self.cleaned_data['email']
        if not User.objects.filter(email__iexact=email_id, is_active=True).exists():
            raise forms.ValidationError("Email address not exists.")

        return email_id

class CustomizeSetNewPassword(SetPasswordForm):
    new_password1 = forms.CharField(label="New Password")
    new_password2 = forms.CharField(label="Confirm Password")

    def clean_new_password2(self):
        new_password1 = self.cleaned_data.get('new_password1')
        new_password2 = self.cleaned_data.get('new_password2')
        if new_password1 != new_password2:
            raise forms.ValidationError("Password doesn't match, please try again.")
