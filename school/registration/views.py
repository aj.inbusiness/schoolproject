from django.shortcuts import render
from django.views import View
from django.http import request, HttpResponse
from .forms import RegistrationForm, UserProfileEditForm, ChangePasswordForm, PasswordResetForm
from django.contrib.sites.shortcuts import get_current_site
from django.shortcuts import render, redirect
from django.contrib.auth import login, authenticate, update_session_auth_hash, logout
from django.utils.encoding import force_bytes, force_text
from django.utils.http import urlsafe_base64_encode, urlsafe_base64_decode
from django.template.loader import render_to_string
from .tokens import account_activation_token
from django.core.mail import EmailMessage
from django.contrib.auth.models import User
from django.contrib import messages
from django.http import HttpResponseRedirect
from .models import UserSecretToken

class UserRegistration(View):

    def get(self, request):
        if request.user.is_anonymous:
            context = {'registration': RegistrationForm}
            return render(request, template_name='user-registration.html', context=context)
        else:
            return redirect("account:profile")

    def post(self, request):
        form = RegistrationForm(request.POST)
        if form.is_valid():
            user = form.save(commit=False)
            user.is_active = False
            user.save()
            username = form.cleaned_data['username']
            to_email = form.cleaned_data['email']
            if User.objects.filter(username=username).exists():
                if sendEmailVerificationMail(user, request, to_email):
                    context = {'message': 'Account has been created, please check your mail for further details.'}
                else:
                    context = {'message': 'Problem in sending email verification .'}
                return render(request, template_name='message-handler.html', context=context)
            else:
                context = {'message': 'Sorry something went wrong, please try again.'}
                return render(request, template_name='message-handler.html', context=context)
        else:
            context = {'registration': form}
            return render(request, template_name="user-registration.html", context=context)

class UserProfileEditPage(View):

    def get(self, request):
        editForm = UserProfileEditForm(instance=request.user)
        context = {"edit_profile_form": editForm}
        return render(request, template_name="edit-user-profile.html", context=context)

    def post(self, request):
        edit_form = UserProfileEditForm(request.POST, instance=request.user)
        #return HttpResponse(request.user.email)
        if edit_form.is_valid():
            user = User.objects.get(id=self.request.user.id)
            if edit_form.cleaned_data['email'] != user.email:
                if sendEmailVerificationMail(user, request, edit_form.cleaned_data['email']):
                    user_profile = edit_form.save(commit=False)
                    user_profile.is_active = False
                    user_profile.save()
                    logout(request)
                    context = {'message': 'Your profile has been updated successfully, please check your mail for further detail, since you have changed you email address.'}
                    return render(request, template_name='message-handler.html', context=context)
            else:
                edit_form.save()
            return redirect("account:profile")
        else:
            #edit_form = UserProfileEditForm(request.POST)
            context = {"edit_profile_form": edit_form}
            return render(request, template_name="edit-user-profile.html", context=context)

def activate(request, uidb64, token, secret_token):
    try:
        uid = force_text(urlsafe_base64_decode(uidb64))
        user = User.objects.get(pk=uid)
    except(TypeError, ValueError, OverflowError, User.DoesNotExist):
        user = None
    if user is not None and account_activation_token.check_token(user, token) and account_activation_token.check_secret_token(uid, secret_token) and user.is_active == False:
        user.is_active = True
        user.save()
        secret_token = UserSecretToken.objects.get(user_id=uid)
        secret_token.is_expired = True
        secret_token.save()
        context = {'message': 'Thank you for your email confirmation. Now you can login your account.'}
        return render(request, template_name='message-handler.html', context=context)
        #user.backend = 'django.contrib.auth.backends.ModelBackend'
    else:
        context = {'message': 'Invalid Request.'}
        return render(request, template_name='message-handler.html', context=context)

class ChangePasswordPage(View):

    def get(self, request):
        change_password_form = ChangePasswordForm(request.user)
        context = {'change_password_form': change_password_form}
        return render(request, template_name='change-password.html', context=context)
    
    def post(self, request):
        change_password_form = ChangePasswordForm(request.user, request.POST)
        if change_password_form.is_valid():
            user = change_password_form.save()
            update_session_auth_hash(request, user)
            messages.success(request, 'Password has been changed successfully.')
            return redirect('accounts:profile')
            #return HttpResponseRedirect(reverse('registration:profile'))
        else:
            context = {'change_password_form':change_password_form}
            return render(request, template_name="change-password.html", context=context)

class ResetPasswordPage(View):
    def get(self, request):
        form = PasswordResetForm(request.post)



def sendEmailVerificationMail(user, request, to_email):
    current_site = get_current_site(request)
    mail_subject = 'Confirm Your Email'
    message = render_to_string('account-activation-email.html', {
        'user': user,
        'domain': current_site.domain,
        'uid': urlsafe_base64_encode(force_bytes(user.pk)),
        'token': account_activation_token.make_token(user),
        'secret_token': account_activation_token.make_secret_token(user)
    })
    email = EmailMessage(mail_subject, message, to=[to_email])
    email.content_subtype = "html"
    if email.send():
        return True
    else:
        return False
