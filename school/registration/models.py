from django.db import models
from django.contrib.auth.models import User
from django.db.models.signals import post_save
from django.dispatch import receiver

class Country(models.Model):
    name = models.CharField(max_length=30)

    def __str__(self):
        return self.name

class State(models.Model):
    country = models.ForeignKey(Country, on_delete=models.CASCADE)
    name = models.CharField(max_length=35)

    def __str__(self):
        return self.name

class UserProfileModel(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    dob = models.DateField('DOB', max_length=10)
    gender_choice = [("male","Male"), ("female", "Female")]
    gender = models.CharField('Gender', max_length=6, choices=gender_choice)
    address = models.CharField(max_length=255, null=True)
    country = models.ForeignKey(Country, on_delete=models.SET_NULL, null=True)
    state = models.ForeignKey(State, on_delete=models.SET_NULL, null=True)

    def user_directory_path(instance, filename):
        # file will be uploaded to MEDIA_ROOT / user_<id>/<filename>
        return 'user_{0}/{1}'.format(instance.user.id, filename)

    profile_pic = models.ImageField(verbose_name='Profile Picture', upload_to=user_directory_path, null=True, blank=True)

    def __str__(self):
        self.user.username

class Profile(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    email_confirmed = models.BooleanField(default=False)
    # other fields...

class UserSecretToken(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    secret_token = models.CharField(max_length=255,null=True)
    is_expired = models.BooleanField(default=False)
    created_on = models.DateTimeField(auto_now_add=True, null=True)
