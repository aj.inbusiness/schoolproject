from django.contrib import admin
from django.urls import path, include
from django.conf.urls import url
from .views import UserRegistration, activate, UserProfileEditPage, ChangePasswordPage
from django.contrib.auth import views as auth_views
from django.contrib.auth.decorators import login_required
from .forms import CustomizeForgotPassword, CustomizeSetNewPassword, CustomizeChangePassword, CustomizeLogin

app_name= 'account'

urlpatterns = [
    path('registration/', UserRegistration.as_view(), name='sign-up'),
    path('email-verification/<uidb64>/<token>/<secret_token>', activate, name='activate'),
    path('login/', auth_views.LoginView.as_view(redirect_authenticated_user=True, template_name="login.html", form_class=CustomizeLogin), name="sign-in"),
    path('logout/', auth_views.LogoutView.as_view(next_page='account:sign-in'), name="logout"),
    path('profile/', login_required(UserProfileEditPage.as_view()), name="profile"),
	path('change-password/', login_required(auth_views.PasswordChangeView.as_view(template_name="change-password.html", form_class=CustomizeChangePassword, success_url="/accounts/change-password-done")), name='change-password'),
	path('change-password-done/', login_required(auth_views.PasswordChangeDoneView.as_view(template_name="change-password-done.html")), name='change-password-done'),
    #path('change-password/', login_required(ChangePasswordPage.as_view()), name='change-password'),
    url(r'password-reset/', auth_views.PasswordResetView.as_view(template_name='password-reset.html', form_class=CustomizeForgotPassword, email_template_name='password-reset-email.html', success_url='/accounts/password-reset-email-done/'), name="forgot-password"),
    url(r'password-reset-email-done/',auth_views.PasswordResetDoneView.as_view(template_name="password-reset-email-done-message.html"), name="password_reset_done"),
    url(r'password-reset-confirm/(?P<uidb64>[0-9A-Za-z_\-]+)/(?P<token>[0-9A-Za-z]{1,13}-[0-9A-Za-z]{1,20})/$',auth_views.PasswordResetConfirmView.as_view(template_name="set-new-password.html", form_class=CustomizeSetNewPassword, success_url='/accounts/password-reset-complete'), name='password_reset_confirm'),
    url(r'password-reset-complete/',auth_views.PasswordResetCompleteView.as_view(template_name="password-reset-complete.html"), name="password_reset_complete"),
]