"""
    smsApp URL Configuration
"""

from django.urls import path
from payment.views import PaymentPage
from django.views.generic import TemplateView
from django.contrib.auth.decorators import login_required

urlpatterns = [
    path('payment/', login_required(PaymentPage.as_view()))
]