# Generated by Django 3.0.4 on 2020-03-22 22:07

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('sms', '0005_auto_20200322_2146'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='phonenumber',
            name='created',
        ),
        migrations.AlterField(
            model_name='phonenumber',
            name='created_at',
            field=models.DateField(auto_now_add=True, null=True),
        ),
    ]
