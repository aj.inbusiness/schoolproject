# Generated by Django 3.0.4 on 2020-03-26 19:44

from django.db import migrations
import phonenumber_field.modelfields


class Migration(migrations.Migration):

    dependencies = [
        ('sms', '0009_auto_20200326_1935'),
    ]

    operations = [
        migrations.AlterField(
            model_name='phonenumber',
            name='phoneNumber',
            field=phonenumber_field.modelfields.PhoneNumberField(max_length=10, null=True, region=None, unique=True, verbose_name='Phone Number'),
        ),
    ]
