from django.contrib import admin
from django.contrib.auth.models import Group
from rangefilter.filter import DateRangeFilter, DateTimeRangeFilter
from .models import PhoneNumber

admin.site.site_header = 'School Admin'
@admin.register(PhoneNumber)
class SchoolAdmin(admin.ModelAdmin):
    list_display = ['contactName', 'phoneNumber', 'created_at', 'status']
    list_filter = (('created_at', DateRangeFilter,), ('status'), 'created_at')
    search_fields = ['contactName', 'phoneNumber']
    #change_list_template = 'admin/sms/daterange_filter.html'

    def make_activate(PhoneNumber, request, queryset):
        queryset.update(status='1')
    make_activate.short_description = "Activate selected records"

    def make_deactivate(PhoneNumber, request, queryset):
        queryset.update(status='0')
    make_deactivate.short_description = "Deactivate selected records"

    actions = ['make_activate', 'make_deactivate']

    ordering = [('created_at')]

    def get_actions(self, request):
        actions = super(SchoolAdmin, self).get_actions(request)
        if not request.user.groups.filter(name='editor').exists() and not request.user.is_superuser:
            del actions['make_activate']
            del actions['make_deactivate']
        return actions
#admin.site.register(PhoneNumber, SchoolAdmin)
#admin.site.unregister(Group)
