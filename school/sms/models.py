from django.db import models
from phonenumber_field.modelfields import PhoneNumberField

class PhoneNumber(models.Model):
    contactName = models.CharField('Name', max_length=100, null=True)
    phoneNumber = PhoneNumberField('Phone Number', null=True, unique=True, default='+91')
    STATUS_OPTIONS = [('1','Active'), ('0', 'Inactive')]
    status = models.CharField('Status', max_length=10, null=True, choices=STATUS_OPTIONS)
    created_at = models.DateField('Created On', auto_now_add=True, null=True)

    def __str__(self):
        return str(self.phoneNumber)
