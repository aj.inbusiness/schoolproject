from django import forms
from .models import PhoneNumber

class MessageForm(forms.Form):

        message = forms.CharField(
                                label='Message',
                                max_length=400,
                                widget=forms.Textarea(attrs={'class': 'form-control message', 'id': 'message', 'placeholder': 'Write your mesage here...'}),
                                help_text="Write your mesage here...",
                                )
        numbers = forms.CharField(max_length=6000, widget=forms.Textarea(attrs={'id': 'numbers'}))

class MessageModelForm(forms.ModelForm):
        class Meta():
            model = PhoneNumber
            fields = '__all__'
