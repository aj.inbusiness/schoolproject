"""
    smsApp URL Configuration
"""

from django.urls import path
from sms import views

urlpatterns = [
    path('', views.sendMessageView, name="send Message"),
    path('contacts-autocomplete/', views.searchContactsAutoComplete, name="send Message"),
]