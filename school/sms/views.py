from django.shortcuts import render, redirect
import json
from django.http import HttpResponse, JsonResponse
from .models import PhoneNumber
from django.core import serializers
import urllib.request
import urllib.parse
from django.http import HttpResponseRedirect
from django.contrib import messages
from .form import MessageForm
from .form import MessageModelForm

def sendMessageView(request):
    if request.user.is_authenticated:
        if request.POST:
            form = MessageForm(request.POST)
            if form.is_valid():
                recipients = request.POST['numbers']
                messageContent = 'Dear Student, ' + request.POST['message']
                smsAPIResponse = sendSMS('EC28M2FgjL8-v45PpkRDTOu9xxAjdoKRtesSphgFEV', recipients, 'SARNGI', messageContent)
                jsonResponse = json.loads(smsAPIResponse.decode('utf-8'))
                if jsonResponse['status'] == "success":
                    messages.success(request, 'Message sent successfully.')
                    return HttpResponseRedirect("/")
                else:
                    messages.error(request, 'Message not sent.')
                    data = {'message': messageContent}
                return render(request, "sendMessage.html")
            else:
                messages.error(request, 'Message is empty, please check your inputs.')
                return render(request, "sendMessage.html")
        else:
            form = MessageForm()
            return render(request, "sendMessage.html", {'form': form})
    else:
        return redirect('/admin')

def searchContactsAutoComplete(request):
    phoneNumbers = PhoneNumber.objects.filter(status='1')
    response = []
    for number in phoneNumbers:
        data = {'label': number.contactName, 'value': str(number.phoneNumber)}
        response.append(data)
    return JsonResponse({"contacts": response})

def sendSMS(apikey, numbers, sender, message):
    data = urllib.parse.urlencode({'apikey': apikey, 'numbers': numbers, 'message': message, 'sender': sender})
    data = data.encode('utf-8')
    request = urllib.request.Request("https://api.textlocal.in/send/?")
    smsRes = urllib.request.urlopen(request, data)
    response = smsRes.read()
    return (response)