$(document).ready(function() {
    $.ajax({
        type:"GET",
        url: "/contacts-autocomplete",
        data:{post_id: 'catid'},
        success: function(response) {
        /* auto complete */
          var autocomplete = new SelectPure(".autocomplete-select", {
            options: response.contacts,
            value: '',
            multiple: true,
            autocomplete: true,
            icon: "fa fa-times",
            onChange: value => {
                $("#numbers").val(value)
                console.log(value)
            },
            classNames: {
              select: "select-pure__select",
              dropdownShown: "select-pure__select--opened",
              multiselect: "select-pure__select--multiple",
              label: "select-pure__label",
              placeholder: "select-pure__placeholder",
              dropdown: "select-pure__options",
              option: "select-pure__option",
              autocompleteInput: "select-pure__autocomplete",
              selectedLabel: "select-pure__selected-label",
              selectedOption: "select-pure__option--selected",
              placeholderHidden: "select-pure__placeholder--hidden",
              optionHidden: "select-pure__option--hidden",
            }
          });
          var resetAutocomplete = function() {
            autocomplete.reset();
          };
      }
    });
});